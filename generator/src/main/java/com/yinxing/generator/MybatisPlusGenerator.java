package com.yinxing.generator;

import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.io.ByteArrayOutputStream;
import java.util.zip.ZipOutputStream;

/**
 * MybatisPlus代码生成工具
 * 可以生成实体类(entity)和mapper(xml)代码
 * 先用此类生成代码，然后再配合ZhiXinCodeGenerator生成Service和Controller的代码
 */
public class MybatisPlusGenerator {

    public static void main(String[] args) {
        AutoGenerator generator = new AutoGenerator();

        //全局配置
        GlobalConfig gc = new GlobalConfig();
        String projectPath = System.getProperty("user.dir");
        //代码生成路径,生成到此项目下在手动拷贝到webapi
        gc.setOutputDir(projectPath + "/generator/src/main/java");
        gc.setAuthor("yinxing");
        gc.setOpen(false);
        gc.setFileOverride(true);
        generator.setGlobalConfig(gc);

        //数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl("jdbc:mysql://127.0.0.1:3306/person_compare?allowPublicKeyRetrieval=true&verifyServerCertificate=false&useUnicode=true&useSSL=false");
        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
        dsc.setUsername("root");
        dsc.setPassword("Sql2008!@#");
//        dsc.setUrl("jdbc:oracle:thin:@42.192.123.31:1521:orcl");
//        dsc.setDriverName("oracle.jdbc.OracleDriver");
//        dsc.setUsername("GVUSER");
//        dsc.setPassword("wxkj2022");
        generator.setDataSource(dsc);

        //生成代码的包配置
        PackageConfig pc = new PackageConfig();
        pc.setParent("com.wxkj.express.spring");
        //是否添加子包
        pc.setEntity("entity");
        pc.setMapper("mapper");
        pc.setXml("mapper");
        pc.setService("service");
        pc.setController("controller");
        generator.setPackageInfo(pc);

        //不生成Controller和Service代码,使用ZhiXinCodeGenerator生成
        TemplateConfig tfg = new TemplateConfig();
        tfg.setController("/template/Controller");
        tfg.setService("/template/Service");
        tfg.setServiceImpl(null);
        tfg.setEntity("/template/entity.java");
        generator.setTemplate(tfg);

        //策略配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setNaming(NamingStrategy.underline_to_camel);
        strategy.setColumnNaming(NamingStrategy.no_change);
        strategy.setEntityLombokModel(true);
        strategy.setRestControllerStyle(true);

        //表名称可用逗号分割一次性生成多张表的代码
        strategy.setInclude("fetch_cardno");
        strategy.setControllerMappingHyphenStyle(true);
        generator.setStrategy(strategy);
        generator.setTemplateEngine(new MyFtlEngine());

        //开始生成代码
        generator.execute();
    }
}
