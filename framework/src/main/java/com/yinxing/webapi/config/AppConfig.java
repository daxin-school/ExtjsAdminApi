package com.yinxing.webapi.config;

import com.yinxing.framework.utils.SpringUtils;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Data
@Configuration
@Component("AppConfig")
public class AppConfig {

    /* ID生成器.机器ID号 1~31 */
    @Value("${yinxing.config.snowflake.datacenterId}")
    private int datacenterId;

    /* ID生成器.工作ID号 1~31 */
    @Value("${yinxing.config.snowflake.workerId}")
    private int workerId;

    /* Netty服务器端口 不能与web端口相同 */
    @Value("${yinxing.config.netty.port}")
    private int nettyPort;

    @Value("${yinxing.config.jwt.secure}")
    private String jwtSecure;

    public static AppConfig getInstance() {
        return SpringUtils.getBean(AppConfig.class);
    }

}
