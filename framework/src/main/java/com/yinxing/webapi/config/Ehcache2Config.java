package com.yinxing.webapi.config;

import org.apache.shiro.cache.ehcache.EhCacheManager;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

@Configuration
public class Ehcache2Config {

    public static final String EHCACHE_CONFIG_NAME = "shiro-ehcache.xml";

    public static final String ACTIVE_SESSION_CACHE_NAME = "shiro-activesession-cache";

    public static final String AUTHORIZATION_CACHE = "shiro-authorization-cache";

    @Bean
    public EhCacheManagerFactoryBean ehCacheFactory(){
        EhCacheManagerFactoryBean ehCacheFactory = new EhCacheManagerFactoryBean();
        ehCacheFactory.setConfigLocation(new ClassPathResource(Ehcache2Config.EHCACHE_CONFIG_NAME));
        ehCacheFactory.setShared(true);
        ehCacheFactory.setAcceptExisting(true);
        return ehCacheFactory;
    }

    @Bean
    public EhCacheManager getEhCacheManager() {
        EhCacheManager ehcacheManager = new EhCacheManager();
        ehcacheManager.setCacheManager(ehCacheFactory().getObject());
        return ehcacheManager;
    }
}
