package com.yinxing.webapi.code.service.sys;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yinxing.framework.mybatis.TemplateService;
import com.yinxing.webapi.code.entity.sys.SysDictype;
import com.yinxing.webapi.code.viewobje.sys.DicTypeTreeNode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class ISysDictypeService extends TemplateService<SysDictype> {

    /**
     * 查询字典全部类型树结构
     */
    public List<DicTypeTreeNode> selectTreeList() {
        Wrapper<SysDictype> wrapper = new QueryWrapper<>();
        List<SysDictype> dictypeList = baseMapper.selectList(wrapper);
        List<DicTypeTreeNode> nodeList = sysDicTypeToTreeNode(dictypeList);
        return convertToTreeList(nodeList);
    }

    /**
     * 查询列表(非树结构),不包含根节点与停用节点
     */
    public List<DicTypeTreeNode> selectNoRootList() {
        LambdaQueryWrapper<SysDictype> wq = new LambdaQueryWrapper<>();
        wq.eq(true, SysDictype::getLeaf_, true);
        wq.eq(true, SysDictype::getStop_, false);
        wq.orderByAsc(SysDictype::getIndex_);
        List<SysDictype> dictypeList = baseMapper.selectList(wq);
        List<DicTypeTreeNode> nodeList = sysDicTypeToTreeNode(dictypeList);
        return nodeList;
    }

    /**
     * 转换为EXT所需格式
     * @param typeList 字典类别列表集合
     */
    public List<DicTypeTreeNode> sysDicTypeToTreeNode(List<SysDictype> typeList) {
        return typeList.stream().map(type -> {
            DicTypeTreeNode node = new DicTypeTreeNode();
            node.setId(type.getId_());
            node.setText(type.getName_());
            node.setCode(type.getCode_());
            node.setType(type.getType_());
            node.setParentId(type.getParentId_());
            node.setIndex(type.getIndex_());
            node.setLeaf(type.getLeaf_());
            node.setStop(type.getStop_());
            node.setExpanded(type.getExpanded_());
            node.setExpandedEdit(type.getExpanded_());
            return node;
        }).collect(Collectors.toList());
    }

    /**
     * 把列表转换为树结构
     * @param nodeList 列表
     * @return 树结构的list
     */
    public List<DicTypeTreeNode> convertToTreeList(List<DicTypeTreeNode> nodeList) {
        List<DicTypeTreeNode> rootList = new ArrayList<>();

        Map<Long, DicTypeTreeNode> entityMap = new HashMap<>(128);
        for (DicTypeTreeNode node: nodeList) {
            entityMap.put(node.getId(), node);
        }

        for (DicTypeTreeNode node : nodeList){
            DicTypeTreeNode parentEntity = entityMap.get(node.getParentId());
            if (parentEntity == null){
                rootList.add(node);
                continue;
            }
            parentEntity.getChildren().add(node);
        }

        return rootList;
    }

    /**
     * 批量新增|更新
     * @param records ext提交的数据格式
     */
    public void insertOrUpdateBatch(List<DicTypeTreeNode> records) {
        for (DicTypeTreeNode record: records) {
            SysDictype dictype = baseMapper.selectById(record.getId());
            if(dictype != null) {
                copyValue(dictype, record);
                baseMapper.updateById(dictype);
            } else {
                dictype = new SysDictype();
                copyValue(dictype, record);
                baseMapper.insert(dictype);
            }
        }
    }

    /**
     * 拷贝属性
     * @param dictype 字典类别数据
     * @param record ext提交的数据格式
     */
    private void copyValue(SysDictype dictype, DicTypeTreeNode record) {
        dictype.setId_(record.getId());
        dictype.setParentId_(record.getParentId());
        dictype.setLeaf_(record.isLeaf());
        dictype.setName_(record.getText());
        dictype.setCode_(record.getCode());
        dictype.setType_(record.getType());
        dictype.setIndex_(record.getIndex());
        dictype.setStop_(record.isStop());
        dictype.setExpanded_(record.isExpandedEdit());
    }

    /**
     * 批量删除
     * @param records ext提交的数据格式
     */
    public void deleteBatch(List<DicTypeTreeNode> records) {
        for (DicTypeTreeNode record: records) {
            baseMapper.deleteById(record.getId());
        }
    }

    /**
     * 启用|禁用
     * @param id 主键ID
     */
    public void stopById(long id) {
        SysDictype record = baseMapper.selectById(id);
        if(record != null) {
            record.setStop_(!record.getStop_());
            this.updateById(record);
        }
    }

}