package com.yinxing.webapi.code.entity.sys;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;


/**
 * <p>
 * 角色权限关系表
 * </p>
 *
 * @author yinxing
 * @since 2022-01-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_rolepermission")
public class SysRolepermission implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * PK
     */
    @TableId("Id_")
    private Long Id_;

    /**
     * 角色ID
     */
    private Long SysRoleId_;

    /**
     * 权限ID
     */
    private Long SysPermissionId_;

}
