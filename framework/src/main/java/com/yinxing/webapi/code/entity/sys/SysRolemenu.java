package com.yinxing.webapi.code.entity.sys;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 角色与菜单关系表
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_rolemenu")
public class SysRolemenu implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * PK
     */
    @TableId("Id_")
    private Long Id_;

    /**
     * 角色ID
     */
    private Long SysRoleId_;

    /**
     * 菜单ID
     */
    private Long SysMenuId_;

}
