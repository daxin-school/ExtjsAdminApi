package com.yinxing.webapi.code.entity.sys;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 字典类别表
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_dictype")
public class SysDictype implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * PK
     */
    @TableId("Id_")
    private Long Id_;

    /**
     * 字典名称
     */
    private String Name_;

    /**
     * 字典编码
     */
    private String Code_;

    /**
     * 字典类型
     */
    private String Type_;

    /**
     * 字典顺序
     */
    private Integer Index_;

    /**
     * 父字典ID
     */
    private Long ParentId_;

    /**
     * 是否叶子节点
     */
    private Boolean Leaf_;

    /**
     * 是否展开
     */
    private Boolean Expanded_;

    /**
     * 是否停用
     */
    private Boolean Stop_;

}
