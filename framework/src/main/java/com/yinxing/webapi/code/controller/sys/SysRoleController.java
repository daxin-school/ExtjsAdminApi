package com.yinxing.webapi.code.controller.sys;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yinxing.framework.domain.R;
import com.yinxing.webapi.code.entity.sys.SysRole;
import com.yinxing.webapi.code.service.sys.ISysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/SysRoleController")
public class SysRoleController {

    @Autowired
    private ISysRoleService service;

    /**
     * 分页查询
     * @param page 分页模型(url?page=1&limit=20)
     * @param params 查询参数 url?page=1&limit=20&key1=value1&key2=value2
     */
    @GetMapping("/selectPage")
    public R selectPage(Page<SysRole> page, @RequestParam Map<String,String> params) {
        LambdaQueryWrapper<SysRole> qw = new LambdaQueryWrapper<>();
        if(params.containsKey("QueryParam_")) {
            qw.eq(SysRole::getRoleName_, params.get("QueryParam_"))
                    .or().eq(SysRole::getRoleCode_, params.get("QueryParam_"));

        }
        qw.orderByAsc(SysRole::getIndex_);
        IPage<SysRole> iPage = service.selectPage(page, qw);
        return R.ok(iPage);
    }

    /**
     * 主键查询
     * @param id 主键
     */
    @GetMapping("/selectById")
    public R selectById(Long id) {
        SysRole record = service.selectById(id);
        return R.ok(record);
    }

    /**
     * 主键删除
     * @param id 主键
     */
    @PostMapping("/deleteById")
    public R deleteById(@RequestParam Long id) {
        service.deleteById(id);
        return R.ok();
    }

    /**
     * 新增
     * @param record 实体类
     */
    @PostMapping("/insert")
    public R insert(SysRole record) {
        service.insert(record);
        return R.ok();
    }

    /**
     * 更新
     * @param record 实体类
     */
    @PostMapping("/update")
    public R update(SysRole record) {
        service.updateById(record);
        return R.ok();
    }

    /**
     * 启用|禁用
     * @param id 主键ID
     */
    @PostMapping("/stopById")
    public R stopById(@RequestParam Long id) {
        service.stopById(id);
        return R.ok();
    }

    /**
     * 查询全部角色(包括禁用状态)
     */
    @GetMapping("/selectAllList")
    public R selectAllList() {
        LambdaQueryWrapper<SysRole> qw = new LambdaQueryWrapper<>();
        qw.orderByAsc(SysRole::getIndex_);
        List<SysRole> roleList = service.selectList(qw);
        return R.ok(roleList);
    }
}
