package com.yinxing.webapi.code.controller.sys;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yinxing.framework.annotation.Json;
import com.yinxing.framework.domain.R;
import com.yinxing.webapi.code.entity.sys.SysTableColumn;
import com.yinxing.webapi.code.service.sys.ISysTableColumnService;
import org.apache.commons.io.IOUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/SysTableColumnController")
public class SysTableColumnController {

    @Autowired
    private ISysTableColumnService service;

    /**
     * 分页查询
     * @param page 分页模型(url?page=1&limit=20)
     * @param params 查询参数 url?page=1&limit=20&key1=value1&key2=value2
     */
    @GetMapping("/selectPage")
    public R selectPage(Page<SysTableColumn> page, @RequestParam Map<String,String> params) {
        QueryWrapper<SysTableColumn> qw = new QueryWrapper<>();
        IPage<SysTableColumn> iPage = service.selectPage(page, qw);
        return R.ok(iPage);
    }

    /**
     * 查询表中所有字段
     * @param tableId
     */
    @GetMapping("/selectListByTableId")
    public R selectListByTableId(@RequestParam Long tableId) {
        List<SysTableColumn> list = service.selectListByTableId(tableId);
        return R.ok(list);
    }

    /**
     * 与数据库同步
     * @param tableId
     */
    @GetMapping("/syncColumnList")
    public R syncColumnList(@RequestParam Long tableId) {
        service.syncColumnList(tableId);
        return R.ok();
    }

    /**
     * 批量更新
     * @param records 数据集合
     */
    @PostMapping("/updateBatch")
    public R updateBatch(@Json List<SysTableColumn> records) {
        service.updateBatch(records);
        return R.ok();
    }

    /**
     * 主键查询
     * @param id 主键
     */
    @GetMapping("/selectById")
    public R selectById(long id) {
        SysTableColumn record = service.selectById(id);
        return R.ok(record);
    }

    /**
     * 主键删除
     * @param id 主键
     */
    @PostMapping("/deleteById")
    public R deleteById(String id) {
        service.deleteById(id);
        return R.ok();
    }

    /**
     * 新增
     * @param record 实体类
     */
    @PostMapping("/insert")
    public R insert(SysTableColumn record) {
        service.insert(record);
        return R.ok();
    }

    /**
     * 更新
     * @param record 实体类
     */
    @PostMapping("/update")
    public R update(SysTableColumn record) {
        service.updateById(record);
        return R.ok();
    }
}
