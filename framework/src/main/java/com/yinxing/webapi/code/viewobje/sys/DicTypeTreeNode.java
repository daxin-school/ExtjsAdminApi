package com.yinxing.webapi.code.viewobje.sys;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class DicTypeTreeNode {
    private long id;
    private long parentId;
    private int index;
    private String text;
    private String code;
    private String type;
    private boolean leaf;
    private boolean expanded;
    private boolean stop;
    private boolean expandedEdit;
    private List<DicTypeTreeNode> children = new ArrayList<>(0);
}
