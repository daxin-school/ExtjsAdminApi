package com.yinxing.webapi.code.entity.sys;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * <p>
 * 
 * </p>
 *
 * @author yinxing
 * @since 2022-01-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_role")
public class SysRole implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * PK
     */
    @TableId("Id_")
    private Long Id_;

    /**
     * 角色名称
     */
    private String RoleName_;

    /**
     * 角色编码
     */
    private String RoleCode_;

    /**
     * 显示顺序
     */
    private Integer Index_;

    /**
     * 是否禁用
     */
    private Boolean Stop_;

    /**
     * 创建时间
     */
    private LocalDateTime CreateTime_;

}
