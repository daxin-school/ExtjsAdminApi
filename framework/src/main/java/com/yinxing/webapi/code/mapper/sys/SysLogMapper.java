package com.yinxing.webapi.code.mapper.sys;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.yinxing.webapi.code.entity.sys.SysLog;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yinxing
 * @since 2022-01-31
 */
public interface SysLogMapper extends BaseMapper<SysLog> {

    IPage<SysLog> selectMyPage(IPage<SysLog> page, @Param(Constants.WRAPPER) QueryWrapper<SysLog> queryWrapper);
}
