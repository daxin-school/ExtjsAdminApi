package com.yinxing.webapi.code.viewobje.sys;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class DicTreeNode {
    private long id;
    private long parentId;
    private int index;
    private String text;
    private String code;
    private long sysDicTypeId;
    private String sysDicTypeCode;
    private boolean leaf;
    private boolean expanded;
    private boolean stop = false;
    private boolean expandedEdit = false;
    private List<DicTreeNode> children = new ArrayList<>(0);
}
