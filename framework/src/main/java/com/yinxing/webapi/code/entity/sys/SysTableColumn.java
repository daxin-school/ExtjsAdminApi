package com.yinxing.webapi.code.entity.sys;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 此类所有字段全部手动改成public
 * 因为lombok的原因生成的get方法freemaker拿不到值
 * </p>
 *
 * @author yinxing
 * @since 2022-01-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_table_column")
public class SysTableColumn implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * PK
     */
    @TableId("Id_")
    public Long Id_;

    /**
     * sys_table表ID
     */
    public Long SysTableId_;

    /**
     * 列名称
     */
    public String Name_;

    /**
     * 数据类型
     */
    public String DataType_;

    /**
     * 列说明
     */
    public String Comment_;

    /**
     * 列序号
     */
    public Integer Position_;

    /**
     * 是否插入(在InsertWindow中)
     */
    public Boolean IsInsert_;

    /**
     * 是否显示(在EditWindow中)
     */
    public Boolean IsUpdate_;

    /**
     * 是否显示(在GridList中)
     */
    public Boolean IsList_;

    /**
     * 是否显示(在SearchWindow中)
     */
    public Boolean IsQuery_;

    /**
     * 是否显示(在ActionPanel中)
     */
    public Boolean IsQueryAction_;

    /**
     * 是否必填(新增和编辑时)
     */
    public Boolean IsRequired_;

    /**
     * 是否显示(在DetailWindow中)
     */
    public Boolean IsDetail_;

    /**
     * 控件类型
     */
    public String Xtype_;

    /**
     * 字典类型编码
     */
    public String SysDicTypeCode_;

}
