package com.yinxing.webapi.code.service.sys;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yinxing.framework.mybatis.TemplateService;
import com.yinxing.netty.utils.ExtPushMessage;
import com.yinxing.netty.utils.ServerUtils;
import com.yinxing.webapi.code.entity.sys.SysMessage;
import com.yinxing.webapi.code.entity.sys.SysMessageQuery;
import com.yinxing.webapi.code.mapper.sys.SysMessageMapper;
import com.yinxing.webapi.shiro.LoginUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Slf4j
@Service
@Transactional
public class ISysMessageService extends TemplateService<SysMessage> {

    private SysMessageMapper getMapper() {
        return (SysMessageMapper) baseMapper;
    }

    /**
     * 连表分页查询
     * 返回类型为SysUserMessageQuery 扩展了连表字段
     */
    public IPage<SysMessageQuery> selectMyPageList(IPage<SysMessage> page, QueryWrapper<SysMessage> qw) {
        //查询当前登录账户的消息
        qw.eq("msg.SysUserId_", LoginUser.current().getUserId());
        return getMapper().selectMyPageList(page, qw);
    }

    /**
     * 新增消息
     * @param message
     */
    public void insertMessage(SysMessage message) {
        message.setCreateTime_(LocalDateTime.now());
        message.setIsRead_(false);
        super.insert(message);
        this.pushUnReadMessageCount(message.getSysUserId_());
    }

    /**
     * 给用户推未读送消息数量
     * @param sysUserId
     */
    public void pushUnReadMessageCount(Long sysUserId) {
        long unReadMsgCount = selectUnReadMessageCountByUserId(sysUserId);
        ExtPushMessage pushMessage = new ExtPushMessage(ExtPushMessage.MessageType.SYS_MESSAGE);
        pushMessage.put("unReadMsgCount", unReadMsgCount);
        ServerUtils.sendMessageToUser(sysUserId, pushMessage);
    }

    /**
     * 查询用户未读消息数量
     * @param sysUserId 用户ID
     */
    public long selectUnReadMessageCountByUserId(Long sysUserId) {
        LambdaQueryWrapper<SysMessage> qw = new LambdaQueryWrapper<>();
        qw.eq(SysMessage::getSysUserId_, sysUserId);
        qw.eq(SysMessage::getIsRead_, false);
        return baseMapper.selectCount(qw);
    }

    /**
     * 查询消息(把消息更新为已读)
     * @param id PK
     */
    public SysMessage selectDetailById(Long id) {
        SysMessage message = super.selectById(id);
        if(message != null) {
            if(message.getIsRead_() == null || !message.getIsRead_()) {
                //未读更新为已读
                message.setIsRead_(true);
                message.setReadTime_(LocalDateTime.now());
                super.updateById(message);
                this.pushUnReadMessageCount(message.getSysUserId_());
            }
        }
        return message;
    }

    /**
     * 删除数据然后推送未读消息数量
     * @param id 消息ID
     */
    public void deleteByIdAndPush(Long id) {
        super.deleteById(id);
        this.pushUnReadMessageCount(LoginUser.current().getUserId());
    }

    /**
     * 批量删除数据然后推送未读消息数量
     * @param ids 消息ID集合
     */
    public void deleteBatchByIdAndPush(Long[] ids) {
        super.deleteBatchById(ids);
        this.pushUnReadMessageCount(LoginUser.current().getUserId());
    }
}