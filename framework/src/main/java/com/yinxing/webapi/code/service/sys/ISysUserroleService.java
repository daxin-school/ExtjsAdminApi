package com.yinxing.webapi.code.service.sys;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yinxing.framework.mybatis.TemplateService;
import com.yinxing.webapi.code.entity.sys.SysUserrole;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Service
@Transactional
public class ISysUserroleService extends TemplateService<SysUserrole> {

    /**
     * 绑定用户和角色关系(编辑用户时)
     * 先删除旧关系 在绑定新的关系
     * @param userId 用户ID
     * @param roles 角色ID集合
     */
    public void updateUserRoles(Long userId, Long[] roles) {
        //删除旧的绑定关系
        this.deleteUserAllRoles(userId);
        //重新插入绑定关系
        if(roles != null && roles.length > 0) {
            for (int i = 0; i < roles.length; i++) {
                this.insert(userId, roles[i]);
            }
        }
    }

    /**
     * 查询用户绑定的角色ID集合
     * @param userId 用户id
     */
    public Long[] selectUserRoles(Long userId) {
        LambdaQueryWrapper<SysUserrole> qw = new LambdaQueryWrapper<>();
        qw.eq(SysUserrole::getSysUserId_, userId);
        List<SysUserrole> sysUserroleList = selectList(qw);
        Long[] roleIds = new Long[sysUserroleList.size()];
        for (int i = 0; i < sysUserroleList.size() ; i++) {
            roleIds[i] = sysUserroleList.get(i).getSysRoleId_();
        }
        return roleIds;
    }

    /**
     * 删除用户绑定的全部角色
     * @param userId 用户ID
     */
    public void deleteUserAllRoles(Long userId) {
        LambdaQueryWrapper<SysUserrole> qw = new LambdaQueryWrapper<>();
        qw.eq(SysUserrole::getSysUserId_, userId);
        super.delete(qw);
    }

    /**
     * 绑定用户与角色
     * @param sysUserId 用户id
     * @param sysRoleId 角色id
     */
    public void insert(long sysUserId, long sysRoleId) {
        //先查询一次 防止重复绑定
        if(this.selectUserRole(sysUserId, sysRoleId) == null) {
            SysUserrole userrole = new SysUserrole();
            userrole.setSysUserId_(sysUserId);
            userrole.setSysRoleId_(sysRoleId);
            super.insert(userrole);
        }
    }

    /**
     * 查询用户角色绑定信息
     * @param sysUserId 用户id
     * @param sysRoleId 角色id
     */
    public SysUserrole selectUserRole(long sysUserId, long sysRoleId) {
        LambdaQueryWrapper<SysUserrole> qw = new LambdaQueryWrapper<>();
        qw.eq(SysUserrole::getSysUserId_, sysUserId);
        qw.eq(SysUserrole::getSysRoleId_, sysRoleId);
        return super.selectOne(qw);
    }

    /**
     * 解绑用户与角色
     * @param sysUserId 用户id
     * @param sysRoleId 角色id
     */
    public void delete(long sysUserId, long sysRoleId) {
        LambdaQueryWrapper<SysUserrole> qw = new LambdaQueryWrapper<>();
        qw.eq(SysUserrole::getSysUserId_, sysUserId);
        qw.eq(SysUserrole::getSysRoleId_, sysRoleId);
        super.delete(qw);
    }

    /**
     * 根据角色ID删除全部数据
     * @param sysRoleId 角色ID
     */
    public void deleteByRoleId(long sysRoleId) {
        LambdaQueryWrapper<SysUserrole> qw = new LambdaQueryWrapper<>();
        qw.eq(SysUserrole::getSysRoleId_, sysRoleId);
        super.delete(qw);
    }
}