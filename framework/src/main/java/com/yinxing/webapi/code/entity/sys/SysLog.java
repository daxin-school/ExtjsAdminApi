package com.yinxing.webapi.code.entity.sys;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * <p>
 * 
 * </p>
 *
 * @author yinxing
 * @since 2022-01-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_log")
@ToString
public class SysLog implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * PK
     */
    @TableId("Id_")
    private Long Id_;

    /**
     * 记录时间
     */
    private LocalDateTime LogTime_;

    /**
     * 请求IP
     */
    private String RequestHost_;

    /**
     * 请求路径
     */
    private String RequestUrl_;

    /**
     * 操作名称
     */
    private String ActionName_;

    /**
     * Http方式
     */
    private String RequestMethod_;

    /**
     * 请求参数JSON
     */
    private String RequestParams_;

    /**
     * 执行时间毫秒
     */
    private Integer RunTime_;

    /**
     * 返回结果JSON
     */
    private String ReturnValue_;

    /**
     * 是否异常
     */
    private Boolean Exception_;

    /**
     * 异常类型
     */
    private String ExceptionClass_;

    /**
     * 用户ID
     */
    private Long UserId_;

    /**
     * 用户行吗
     */
    private String UserName_;

}
