package com.yinxing.webapi.code.viewobje.sys;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FusionchartVo {

    private String label;
    private String value;
}
