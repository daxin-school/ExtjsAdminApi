package com.yinxing.webapi.code.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yinxing.webapi.code.entity.sys.SysRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yinxing
 * @since 2022-01-23
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

    List<SysRole> selectRolesBySysUserId(@Param("sysUserId") long sysUserId);
}
