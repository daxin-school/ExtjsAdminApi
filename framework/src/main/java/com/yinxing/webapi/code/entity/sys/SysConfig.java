package com.yinxing.webapi.code.entity.sys;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * <p>
 * 
 * </p>
 *
 * @author yinxing
 * @since 2022-03-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_config")
public class SysConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("Id_")
    private Long Id_;

    /**
     * 配置名称
     */
    private String ConfigName_;

    /**
     * 配置键名
     */
    private String ConfigKey_;

    /**
     * 配置键值
     */
    private String ConfigValue_;

    /**
     * 系统内置(内置的不允许删除和修改ConfigKey_)
     */
    private Boolean SystemIn_;

    /**
     * 备注
     */
    private String Remark_;

    /**
     * 最后更新时间
     */
    private LocalDateTime UpdateTime_;


}
