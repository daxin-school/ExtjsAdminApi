package com.yinxing.webapi.code.entity.sys;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * <p>
 * 
 * </p>
 *
 * @author yinxing
 * @since 2022-02-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_message")
public class SysMessage implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * PK
     */
    @TableId("Id_")
    private Long Id_;

    /**
     * 用户表ID
     */
    private Long SysUserId_;

    /**
     * 创建时间
     */
    private LocalDateTime CreateTime_;

    /**
     * 创建用户
     */
    private String CreateUser_;

    /**
     * 消息类型ID
     */
    private Long SysDicId_;

    /**
     * 消息内容
     */
    private String Message_;

    /**
     * 是否阅读
     */
    private Boolean IsRead_;

    /**
     * 阅读时间
     */
    private LocalDateTime ReadTime_;

}
