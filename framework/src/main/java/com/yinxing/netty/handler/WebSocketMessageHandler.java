package com.yinxing.netty.handler;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ChannelHandler.Sharable
public class WebSocketMessageHandler extends SimpleChannelInboundHandler<TextWebSocketFrame> {

    public static final WebSocketMessageHandler INSTANCE = new WebSocketMessageHandler();

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, TextWebSocketFrame text) {
        log.info(text.text());
    }
}
