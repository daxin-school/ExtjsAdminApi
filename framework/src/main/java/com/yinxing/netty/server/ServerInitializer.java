package com.yinxing.netty.server;

import com.yinxing.netty.handler.ExceptionHandler;
import com.yinxing.netty.handler.WebSocketEventHandler;
import com.yinxing.netty.handler.WebSocketMessageHandler;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.stream.ChunkedWriteHandler;
import io.netty.handler.timeout.ReadTimeoutHandler;

import java.util.concurrent.TimeUnit;

public class ServerInitializer extends ChannelInitializer {

    @Override
    protected void initChannel(Channel ch) {
        ChannelPipeline pipeline = ch.pipeline();
        pipeline.addLast("readTimeOut", new ReadTimeoutHandler(30, TimeUnit.SECONDS));
        pipeline.addLast("http-codec", new HttpServerCodec());
        pipeline.addLast("http-chunked", new ChunkedWriteHandler());
        pipeline.addLast("aggregator", new HttpObjectAggregator(Integer.MAX_VALUE));
        pipeline.addLast("webSocketHandshake", new WebSocketServerProtocolHandler("/yinxing-web/websocket", true));
        pipeline.addLast("wsEventHandler", WebSocketEventHandler.INSTANCE);
        pipeline.addLast("wsMessageHandler", WebSocketMessageHandler.INSTANCE);
        pipeline.addLast("exceptionHandler", ExceptionHandler.INSTANCE);
    }

}
