package com.yinxing.framework.system;

import com.yinxing.framework.utils.HttpUtils;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.stereotype.Component;

@Component
public class Resource implements DisposableBean {

    @Override
    public void destroy() {
        HttpUtils.destroy();
    }
}
