package com.yinxing.framework.system;

import lombok.Data;
import lombok.ToString;

/**
 * CPU相关信息
 */
@Data
@ToString
public class Cpu {
    /**
     * 核心数
     */
    private int cpuNum;

    /**
     * CPU空闲率
     */
    private String totalFree;

    /**
     * CPU系统使用率
     */
    private String sys;

    /**
     * CPU用户使用率
     */
    private String used;

    /**
     * CPU当前等待率
     */
    private String wait;

}
