package com.yinxing.framework.system;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
public class NameData {

    private String name;
    private int data;
}
