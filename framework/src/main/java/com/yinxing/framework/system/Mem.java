package com.yinxing.framework.system;

import lombok.Data;
import lombok.ToString;

/**
 * 內存相关信息
 */
@Data
@ToString
public class Mem {
    /**
     * 内存总量
     */
    private String total;

    /**
     * 已用内存
     */
    private String used;

    /**
     * 使用率
     */
    private String usedRate;

    /**
     * 剩余内存
     */
    private String free;

}
