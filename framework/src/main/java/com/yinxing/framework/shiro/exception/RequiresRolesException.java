package com.yinxing.framework.shiro.exception;

import org.apache.shiro.ShiroException;

public class RequiresRolesException extends ShiroException {

    public RequiresRolesException() {
        super();
    }

    public RequiresRolesException(String message) {
        super(message);
    }


    public RequiresRolesException(Throwable cause) {
        super(cause);
    }

    public RequiresRolesException(String message, Throwable cause) {
        super(message, cause);
    }
}
