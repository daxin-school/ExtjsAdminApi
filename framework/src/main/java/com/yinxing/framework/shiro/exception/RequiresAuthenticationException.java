package com.yinxing.framework.shiro.exception;

import org.apache.shiro.ShiroException;

public class RequiresAuthenticationException extends ShiroException {
    public RequiresAuthenticationException() {
        super();
    }

    public RequiresAuthenticationException(String message) {
        super(message);
    }


    public RequiresAuthenticationException(Throwable cause) {
        super(cause);
    }

    public RequiresAuthenticationException(String message, Throwable cause) {
        super(message, cause);
    }
}
