package com.yinxing.framework.shiro.interceptor;

import com.yinxing.framework.shiro.exception.RequiresAuthenticationException;
import org.apache.shiro.authz.UnauthenticatedException;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.aop.AuthenticatedAnnotationHandler;

import java.lang.annotation.Annotation;

public class MyAuthenticatedAnnotationHandler extends AuthenticatedAnnotationHandler {

    public void assertAuthorized(Annotation a) throws UnauthenticatedException {
        if (a instanceof RequiresAuthentication && !getSubject().isAuthenticated() ) {
            throw new RequiresAuthenticationException("用户未登录");
        }
    }
}
