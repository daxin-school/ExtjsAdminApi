package com.yinxing.framework.shiro.interceptor;

import org.apache.shiro.aop.AnnotationResolver;
import org.apache.shiro.authz.aop.*;
import org.apache.shiro.spring.aop.SpringAnnotationResolver;
import org.apache.shiro.spring.security.interceptor.AopAllianceAnnotationsAuthorizingMethodInterceptor;

import java.util.ArrayList;
import java.util.List;

public class MyMethodInterceptor extends AopAllianceAnnotationsAuthorizingMethodInterceptor {

    public MyMethodInterceptor() {
        List<AuthorizingAnnotationMethodInterceptor> interceptors =
                new ArrayList<AuthorizingAnnotationMethodInterceptor>(5);

        AnnotationResolver resolver = new SpringAnnotationResolver();

        //interceptors.add(new RoleAnnotationMethodInterceptor(resolver));
        //interceptors.add(new PermissionAnnotationMethodInterceptor(resolver));
        //interceptors.add(new AuthenticatedAnnotationMethodInterceptor(resolver));
        //interceptors.add(new GuestAnnotationMethodInterceptor(resolver));
        //interceptors.add(new UserAnnotationMethodInterceptor(resolver));

        //传入MyRoleAnnotationHandler 抛出精细化异常信息
        RoleAnnotationMethodInterceptor role = new RoleAnnotationMethodInterceptor(resolver);
        role.setHandler(new MyRoleAnnotationHandler());
        interceptors.add(role);

        //传入MyPermissionAnnotationHandler 抛出精细化异常信息
        PermissionAnnotationMethodInterceptor permission = new PermissionAnnotationMethodInterceptor(resolver);
        permission.setHandler(new MyPermissionAnnotationHandler());
        interceptors.add(permission);

        //传入MyAuthenticatedAnnotationHandler 抛出精细化异常信息
        AuthenticatedAnnotationMethodInterceptor login = new AuthenticatedAnnotationMethodInterceptor(resolver);
        login.setHandler(new MyAuthenticatedAnnotationHandler());
        interceptors.add(login);

        //传入MyUserAnnotationHandler 抛出精细化异常信息
        UserAnnotationMethodInterceptor user = new UserAnnotationMethodInterceptor(resolver);
        user.setHandler(new MyUserAnnotationHandler());
        interceptors.add(user);

        //传入GuestAnnotationMethodInterceptor 抛出精细化异常信息
        GuestAnnotationMethodInterceptor quest = new GuestAnnotationMethodInterceptor(resolver);
        quest.setHandler(new MyGuestAnnotationHandler());
        interceptors.add(quest);

        setMethodInterceptors(interceptors);
    }
}
