package com.yinxing.framework.mybatis;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PageParam {

    @ApiModelProperty(value = "页码")
    protected int page;
    @ApiModelProperty(value = "每页行数")
    protected int limit;
}
