package com.yinxing.framework.utils;


import com.yinxing.framework.exception.ParameterException;
import org.springframework.core.MethodParameter;

import java.util.Map;

public class AppUtils {

    public static void notNull(Map<String, Object> map, String key) {
        if (!map.containsKey(key)) {
            throw new ParameterException("参数不能为空:{" + key + "}");
        }
    }

    public static String getMethodParameterTypes(MethodParameter[] parameters) {
        StringBuilder sb = new StringBuilder("");
        for (int i = 0; i < parameters.length; i++) {
            MethodParameter parameter = parameters[i];
            sb.append(parameter.getParameterType().getSimpleName());
            if(i < parameters.length - 1) {
                sb.append("|");
            }
        }
        return sb.toString();
    }
}
