package com.yinxing.framework.enums;

public enum RCode {

    /**
     * 操作成功
     */
    SUCCESS,

    /**
     * 操作失败
     */
    ERROR,

    /**
     * 登录失败
     */
    LOGIN_FAILURE,

    /**
     * 文件超出限制
     */
    FILE_SIZE_EXCEEDS_LIMIT,

    /**
     * 当前请求方式不主持
     */
    METHOD_NOT_SUPPORTED,

    /**
     * 权限不足
     */
    ACCESS_UNAUTHORIZED,

    /**
     * 账户未认证
     */
    ACCOUNT_UNAUTHENTICATED,

    /**
     * 系统异常
     */
    SYSTEM_ERROR,

    /**
     * 数据源不可用
     */
    DATASOURCE_ERROR,

    /**
     * 参数不合法
     */
    PARAMETER_ILLEGAL,

    /**
     * 服务暂时不可用
     */
    SYSTEM_BUSY,

    /**
     * URL不存在(404)
     */
    NO_HANDLER_FOUND
}
