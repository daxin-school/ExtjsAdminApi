Ext.define('Admin.model.${sysTable.Package_}.${sysTable.ModelName_}Model', {
    extend: 'Ext.data.Model',
    idProperty: 'Id_',

    fields: [
    <#list columnList as column>
        '${column.Name_}'<#if column_has_next>,</#if>
    </#list>
    ]
});
