Ext.define('Admin.view.${sysTable.Package_}.MainView', {
    extend: 'Ext.container.Container',
    xtype: '${xtypePrefix}_mainview',

    layout: {
        align: 'stretch',
        type: 'vbox'
    },

    controller: '${xtypePrefix}_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: '${xtypePrefix}_actionpanel',
                reference: 'actionpanel'
            }, {
                xtype: '${xtypePrefix}_gridlist',
                reference: 'gridlist',
                flex: 1
            }]
        });

        me.callParent(arguments);
    }

});