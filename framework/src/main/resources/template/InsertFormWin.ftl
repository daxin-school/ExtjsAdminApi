Ext.define('Admin.view.${sysTable.Package_}.InsertFormWin', {
    extend: 'Admin.base.BaseFormWindow',
    xtype: '${xtypePrefix}_insertformwin',

    /*视图控制器 定义事件*/
    controller: '${xtypePrefix}_insertformwincontroller',

    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [{
                xtype: 'baseformpanel',
                reference: 'formpanel',
                items: [<#list insertList as column>
                    {
                        xtype: '${column.Xtype_}',
                        <#if column.Xtype_ == "sys_dic_sysdictreepicker">
                        sysDicTypeCode: '${column.SysDicTypeCode_}',
                        </#if>
                        <#if column.Xtype_ == "sys_dic_sysdiccombobox">
                        sysDicTypeCode: '${column.SysDicTypeCode_}',
                        </#if>
                        name : '${column.Name_}',
                        <#if column.IsRequired_ == true>
                        allowBlank: false,
                        </#if>
                        fieldLabel : '${column.Comment_}'
                    }<#if column_has_next>,</#if>
                    </#list>
                ]
            }]
        });
        me.callParent(arguments);
    },

    buttons: [{
        xtype: 'savebutton',
        handler: 'onSave'
    }, {
        xtype: 'cancelbutton',
        handler: 'onCancel'
    }]

});